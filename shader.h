#pragma once

#include <string>

class Shader {
public:
    Shader(const std::string& vertexPath, const std::string& fragmentPath);

    ~Shader();

    void use();

    // int getAttrib(const std::string& name) const;

    // utility uniform functions
    // void setBool(const std::string& name, bool value) const;
    // void setInt(const std::string& name, int value) const;
    // void setFloat(const std::string& name, float value) const;

private:
    std::string readFile(const std::string& file_path, const std::string& name);

    void checkProgramCompileErrors(unsigned int program);
    void checkShaderCompileErrors(unsigned int shader, const std::string& name);

    unsigned int shaderProgram;

    int success;
    char infoLog[1024];

    inline static const std::string vertexName { "VERTEX" };
    inline static const std::string fragmentName { "FRAGMENT" };
};
