#include <glad/glad.h>

#include "large-mesh.h"

#include <GLFW/glfw3.h>

LargeMesh::LargeMesh(int L_, Shader& shader)
    : L(L_)
    , l(L_ - 1)
    , shader(shader)
    , VAO()
    , VBO()
    , EBO()
    , vertices(L)
    , indices(6 * l * l)
{
    float step { 2.f / l };
    for (int y { 0 }; y < L; ++y) {
        for (int x { 0 }; x < L; ++x) {
            vertices.ref(x, y) = Vertex(-1.f + step * x, -1.f + step * y);
        }
    }

    int a { 0 };
    for (int y { 0 }; y < l; ++y) {
        for (int x { 0 }; x < l; ++x) {
            int i { vertices.i(x, y) };
            indices[a++] = i;
            indices[a++] = i + 1;
            indices[a++] = i + L + 1;
            indices[a++] = i;
            indices[a++] = i + L + 1;
            indices[a++] = i + L;
        }
    }

    setupMesh();
}

LargeMesh::~LargeMesh()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

void LargeMesh::bufferVertices()
{
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
}

void LargeMesh::setupMesh()
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    bufferVertices();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

    // position attribute
    // vertex attribute: (location, size, type, normalise, stride, offset)
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);

    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);
}

void LargeMesh::draw()
{
    bufferVertices();

    // indices: (mode, count, type, offset)
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
}
