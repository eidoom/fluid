#pragma once

#include <array>

#include "matrix.h"

class LargeFluid {
private:
    const int N;
    const int n;

    using Grid = LargeSquareMatrix<float>;

public:
    LargeFluid(int N_);

    void tick(float dt);

    const Grid& density() const;

private:
    void addSource(float dt, const Grid& source, Grid& grid);
    void diffuse(float dt, const Grid& grid_prev, Grid& grid, float coeff);
    void advect(float dt, const Grid& grid_prev, Grid& grid, const Grid& u, const Grid& v);
    void project();

    void setBoundary(Grid& grid);
    void setBoundaryVelX();
    void setBoundaryVelY();
    void setBoundaryCorners(Grid& grid);

    Grid dens_prev;
    Grid dens;

    Grid velx_prev;
    Grid velx;

    Grid vely_prev;
    Grid vely;

    const float diff;
    const float visc;

    const int iterations;
};
