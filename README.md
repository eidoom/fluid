# [fluid](https://gitlab.com/eidoom/fluid)
* [Companion post](https://eidoom.gitlab.io/computing-blog/post/fluid)
## Usage
* Dependencies (Fedora 33)
    ```shell
    sudo dnf install mesa-libGL-devel glad glfw-devel
    ```
* Build
    ```shell
    make -j
    ```
* Run
    ```shell
    ./main
    ```
## Reading
* <https://learnopengl.com/Getting-started/Hello-Triangle>
* <https://www.dgp.toronto.edu/public_user/stam/reality/Research/pdf/GDC03.pdf>
## Notes
### Summary
Windowing is provided by `Window` (`window.h`), a wrapper class providing a `C++` API for `C`-based GLFW.
The OpenGL vertex shader `shader.vert` and fragment shader `shader.frag` are read, compiled, and linked into a shader program by wrapper class `Shader` (`shader.h`).
The shaders are written in OpenGL Shading Language (GLSL) and control the GPU rendering.
The pipeline is that we send data to the vertex shader, which processes the vertex positions and then sends its output to the fragment shader, which determines the colours of pixels.

`SmallMesh` (`small-mesh.h`) stores information about the vertices of a surface.
Each vertex has a position and a colour, for which we use `Vertex` (`vertex.h`).
Since we are simulating a 2d fluid, our vertices are a regular grid on a flat rectangle.
On the CPU end, we store this as a 2d array, provided by `Matrix` (`matrix.h`), so the position in this matrix gives the matrix coordinate of the vertex.
This is used to calculate the vertex's normalized device coordinate (NDC), which is the format OpenGL wants.
The CPU sets the colour each tick, while the positions remain constant.

To send this data to the GPU, we set up a:

* Vertex Buffer Object (VBO) to buffer the vertex data
* Element Buffer Object (EBO), which contains a list of indices describing how to draw lines between the vertices to tessellate the surface into triangles
* Vertex Array Object (VAO) to store the vertex attribute information (eg. the vertices in the VBO are xyz position then rgb colour) and point to the VBO and EBO

Each tick, we load the updated vertices into the VBO and ask the GPU to draw our VAO triangles.
Even with a coarse grid, the colour varies smoothly over the surface because the fragment shader interpolates the colours between the three vertices of each triangle.

The class `SmallFluid` (`small-fluid.h`) contains the density and velocity fields of the fluid and member functions describing their evolution.
Each tick, the fluid is updated and the mesh colours set by the density of the fluid using `Colourmap` (`colourmap.h`).
### Large vs small
The `large` and `small` versions of `fluid` and `mesh` are based on `std::vector` and `std::array` respectively.
This is for storing the `mesh` vertices, and the density and velocity fields for the `fluid`.
`small` is in theory faster since it stores data on the stack, while `large` stores it on the heap (the data is statically sized in both cases).
However, if the grid size is too large, using `small` will give a stack buffer overflow, so `large` must be used.
### Instability
The program is unstable.
The density field diverges in the long time limit.
