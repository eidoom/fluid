#pragma once

// glad must be included before GLFW
#include <glad/glad.h>

#include <GLFW/glfw3.h>

class Window {
public:
    Window(int w, int h);

    void tick();

    static void error_callback(int error, const char* description);
    // static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
    // static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

    GLFWwindow* window;

    int width;
    int height;
};
