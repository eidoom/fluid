#include <iostream>

#include "vertex.h"

Vertex::Vertex(const std::array<float, 2>& position_, const std::array<float, 3>& colour_)
    : position(position_)
    , colour(colour_)
{
}

Vertex::Vertex(const std::array<float, 2>& position_)
    : position(position_)
    , colour()
{
}

Vertex::Vertex(const float x, const float y, const float r, const float g, const float b)
    : position({ x, y })
    , colour({ r, g, b })
{
}

Vertex::Vertex(const float x, const float y)
    : position({ x, y })
    , colour()
{
}

Vertex::Vertex()
    : position()
    , colour()
{
}

std::ostream& operator<<(std::ostream& outstream, const Vertex& v)
{
    outstream
        << "rgb(" << v.colour[0] << ", " << v.colour[1] << ", " << v.colour[2] << ") @ "
        << "xy(" << v.position[0] << ", " << v.position[1] << ")";
    return outstream;
}
