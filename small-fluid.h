#pragma once

#include <array>
#include <cstdlib>

#include "matrix.h"

template <int W, int H>
class SmallFluid {
private:
    static constexpr int w { W - 2 };
    static constexpr int p { W - 1 };
    static constexpr int h { H - 2 };
    static constexpr int q { H - 1 };

    using Grid = Matrix<float, W, H>;

public:
    SmallFluid();

    void tick(float dt);

    const Grid& density() const;
    void addSource(int x, int y);
    void addSink(int x, int y);
    void addFan(int x, int y, float vx, float vy);

private:
    void source(float dt, const Grid& source, Grid& grid);
    void diffuse(float dt, const Grid& grid_prev, Grid& grid, float coeff);
    void advect(float dt, const Grid& grid_prev, Grid& grid, const Grid& u, const Grid& v);
    void project();

    void boundary(Grid& grid);
    void boundaryVelX();
    void boundaryVelY();
    void boundaryCorners(Grid& grid);

    Grid dens_prev;
    Grid dens;

    Grid velx_prev;
    Grid velx;

    Grid vely_prev;
    Grid vely;

    const float diff;
    const float visc;

    const int iterations;
    int interaction;
};

// --------------
// implementation
// --------------

template <int W, int H>
SmallFluid<W, H>::SmallFluid()
    : dens_prev()
    , dens()
    , velx_prev()
    , velx()
    , vely_prev()
    , vely()
    , diff(1.f)
    , visc(1e-2f)
    , iterations(20)
    , interaction(1e2f)
{
}

template <int W, int H>
const Matrix<float, W, H>& SmallFluid<W, H>::density() const
{
    return dens;
}

template <int W, int H>
void SmallFluid<W, H>::addSource(int x, int y)
{
    dens.ref(x, y) += interaction;
}

template <int W, int H>
void SmallFluid<W, H>::addSink(int x, int y)
{
    dens.ref(x, y) -= interaction;
}

template <int W, int H>
void SmallFluid<W, H>::addFan(int x, int y, float vx, float vy)
{
    int r { 1 };
    float s { 1e5f };

    for (int X { x }; X < x + r; ++X) {
        vely.ref(X, y) += s;
    }

    // velx.ref(X, y) += vx;
    // vely.ref(x, y) += vy;

    // for (float& v : vely) {
    //     v += 5e2f;
    // }
}

template <int W, int H>
void SmallFluid<W, H>::source(float dt, const Grid& source, Grid& grid)
{
    for (std::size_t i { 0 }; i < source.size(); ++i) {
        grid[i] += dt * source[i];
    }
}

// iterative solver (Gauss-Seidel relaxation) for
// x0[i,j]=x[i,j]-a*(x[i-1,j]+x[i+1,j]+x[i,j-1]+x[i,j+1]-4*x[i,j]);
// https://en.wikipedia.org/wiki/Gauss%E2%80%93Seidel_method
template <int W, int H>
void SmallFluid<W, H>::diffuse(float dt, const Grid& grid_prev, Grid& grid, float coeff)
{
    float a { dt * coeff * w * h };
    float c { 1.f / (1.f + 4.f * a) };
    for (int k { 0 }; k < iterations; ++k) {
        for (int y { 1 }; y <= h; ++y) {
            for (int x { 1 }; x <= w; ++x) {
                grid.ref(x, y) = c * (grid_prev.get(x, y) + a * (grid.get(x, y - 1) + grid.get(x, y + 1) + grid.get(x - 1, y) + grid.get(x + 1, y)));
            }
        }
    }
}

template <int W, int H>
void SmallFluid<W, H>::advect(float dt, const Grid& grid_prev, Grid& grid, const Grid& u, const Grid& v)
{
    float dtx { dt * w };
    float dty { dt * h };
    for (int i { 1 }; i <= h; ++i) {
        for (int j { 1 }; j <= w; ++j) {
            float I { i - dtx * u.get(j, i) };
            float J { j - dty * v.get(j, i) };

            if (I < 0.5f) {
                I = 0.5f;
            } else if (I > h + 0.5f) {
                I = w + 0.5f;
            }

            if (J < 0.5f) {
                J = 0.5f;
            } else if (J > w + 0.5f) {
                J = h + 0.5f;
            }

            int i0 { static_cast<int>(I) };
            int i1 { i0 + 1 };

            int j0 { static_cast<int>(J) };
            int j1 { j0 + 1 };

            float s1 { I - i0 };
            float s0 { 1.f - s1 };
            float t1 { J - j0 };
            float t0 { 1.f - t1 };

            grid.ref(j, i) = s0 * (t0 * grid_prev.get(j0, i0) + t1 * grid_prev.get(j1, i0))
                + s1 * (t0 * grid_prev.get(j0, i1) + t1 * grid_prev.get(j1, i1));
        }
    }
}

template <int W, int H>
void SmallFluid<W, H>::project()
{
    float ax { 1.f / w };
    float ay { 1.f / h };
    for (int y { 1 }; y <= h; ++y) {
        for (int x { 1 }; x <= w; ++x) {
            vely_prev.ref(x, y) = -0.5f * (ax * (velx.get(x, y + 1) - velx.get(x, y - 1)) + ay * (vely.get(x + 1, y) - vely.get(x - 1, y)));
            velx_prev.ref(x, y) = 0.f;
        }
    }
    boundary(vely_prev);
    boundary(velx_prev);
    for (int k { 0 }; k < iterations; ++k) {
        for (int y { 1 }; y <= h; ++y) {
            for (int x { 1 }; x <= w; ++x) {
                velx_prev.ref(x, y) = 0.25f * (vely_prev.get(x, y) + velx_prev.get(x, y - 1) + velx_prev.get(x, y + 1) + velx_prev.get(x - 1, y) + velx_prev.get(x + 1, y));
            }
        }
        boundary(velx_prev);
    }
    for (int y { 1 }; y <= h; ++y) {
        for (int x { 1 }; x <= w; ++x) {
            velx.ref(x, y) -= 0.5f * (velx_prev.get(x, y + 1) - velx_prev.get(x, y - 1)) * w;
            vely.ref(x, y) -= 0.5f * (velx_prev.get(x + 1, y) - velx_prev.get(x - 1, y)) * h;
        }
    }
    boundaryVelX();
    boundaryVelY();
}

template <int W, int H>
void SmallFluid<W, H>::boundary(Grid& grid)
{
    for (int x { 1 }; x <= w; ++x) {
        grid.ref(x, 0) = grid.get(x, 1);
        grid.ref(x, q) = grid.get(x, h);
    }
    for (int y { 1 }; y <= h; ++y) {
        grid.ref(0, y) = grid.get(1, y);
        grid.ref(p, y) = grid.get(w, y);
    }
    boundaryCorners(grid);
}

template <int W, int H>
void SmallFluid<W, H>::boundaryVelX()
{
    for (int x { 1 }; x <= w; ++x) {
        velx.ref(x, 0) = -velx.get(x, 1);
        velx.ref(x, q) = -velx.get(x, h);
    }
    for (int y { 1 }; y <= h; ++y) {
        velx.ref(0, y) = velx.get(1, y);
        velx.ref(p, y) = velx.get(w, y);
    }
    boundaryCorners(velx);
}

template <int W, int H>
void SmallFluid<W, H>::boundaryVelY()
{
    for (int x { 1 }; x <= w; ++x) {
        vely.ref(x, 0) = vely.get(x, 1);
        vely.ref(x, q) = vely.get(x, h);
    }
    for (int y { 1 }; y <= h; ++y) {
        vely.ref(0, y) = -vely.get(1, y);
        vely.ref(p, y) = -vely.get(w, y);
    }
    boundaryCorners(vely);
}

template <int W, int H>
void SmallFluid<W, H>::boundaryCorners(Grid& grid)
{
    grid.ref(0, 0) = 0.5f * (grid.get(1, 0) + grid.get(0, 1));
    grid.ref(0, q) = 0.5f * (grid.get(1, q) + grid.get(0, h));
    grid.ref(p, 0) = 0.5f * (grid.get(w, 0) + grid.get(p, 1));
    grid.ref(p, q) = 0.5f * (grid.get(w, q) + grid.get(p, h));
}

template <int W, int H>
void SmallFluid<W, H>::tick(float dt)
{
    // velocity
    source(dt, velx_prev, velx);
    std::swap(velx_prev, velx);
    diffuse(dt, velx_prev, velx, visc);
    boundaryVelX();

    source(dt, vely_prev, vely);
    std::swap(vely_prev, vely);
    diffuse(dt, vely_prev, vely, visc);
    boundaryVelY();

    project();

    std::swap(velx_prev, velx);
    std::swap(vely_prev, vely);

    advect(dt, velx_prev, velx, velx_prev, vely_prev);
    boundaryVelX();
    advect(dt, vely_prev, vely, velx_prev, vely_prev);
    boundaryVelY();

    project();

    // density
    source(dt, dens_prev, dens);
    std::swap(dens_prev, dens);
    diffuse(dt, dens_prev, dens, diff);
    boundary(dens);
    std::swap(dens_prev, dens);
    advect(dt, dens_prev, dens, velx, vely);
    boundary(dens);
}
