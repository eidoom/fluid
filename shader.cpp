#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>

#include <glad/glad.h>

#include "shader.h"

Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath)
{
    // read the shader source codes
    std::string vertexCode { readFile(vertexPath, vertexName) };
    std::string fragmentCode { readFile(fragmentPath, fragmentName) };

    const char* vShaderCode { vertexCode.c_str() };
    const char* fShaderCode { fragmentCode.c_str() };

    // compile vertex shader
    unsigned int vertexShader { glCreateShader(GL_VERTEX_SHADER) };
    glShaderSource(vertexShader, 1, &vShaderCode, NULL);
    glCompileShader(vertexShader);
    checkShaderCompileErrors(vertexShader, vertexName);

    // compile fragment shader
    unsigned int fragmentShader { glCreateShader(GL_FRAGMENT_SHADER) };
    glShaderSource(fragmentShader, 1, &fShaderCode, NULL);
    glCompileShader(fragmentShader);
    checkShaderCompileErrors(fragmentShader, fragmentName);

    // shader program
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    checkProgramCompileErrors(shaderProgram);

    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

Shader::~Shader()
{
    glDeleteProgram(shaderProgram);
}

void Shader::use()
{
    glUseProgram(shaderProgram);
}

// int Shader::getAttrib(const std::string& name) const
// {
//     return glGetAttribLocation(shaderProgram, name.c_str());
// }

// void Shader::setBool(const std::string& name, bool value) const
// {
//     glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), static_cast<int>(value));
// }

// void Shader::setInt(const std::string& name, int value) const
// {
//     glUniform1i(glGetUniformLocation(shaderProgram, name.c_str()), value);
// }

// void Shader::setFloat(const std::string& name, float value) const
// {
//     glUniform1f(glGetUniformLocation(shaderProgram, name.c_str()), value);
// }

std::string Shader::readFile(const std::string& file_path, const std::string& name)
{
    std::ifstream input_file(file_path);
    if (!input_file.is_open()) {
        std::cerr << "ERROR::SHADER::" << name << "::FILE_NOT_SUCCESFULLY_READ\n"
                  << "Could not open file '" << file_path << "'" << '\n'
                  << "-- --------------------------------------------------- -- " << '\n';
        std::exit(EXIT_FAILURE);
    }
    return std::string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
}

void Shader::checkProgramCompileErrors(unsigned int program)
{
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 1024, NULL, infoLog);
        std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n"
                  << infoLog << '\n'
                  << "-- --------------------------------------------------- -- " << '\n';
        std::exit(EXIT_FAILURE);
    }
}

void Shader::checkShaderCompileErrors(unsigned int shader, const std::string& name)
{
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 1024, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << name << "::COMPILATION_FAILED\n"
                  << infoLog << '\n'
                  << "-- --------------------------------------------------- -- " << '\n';
        std::exit(EXIT_FAILURE);
    }
}
