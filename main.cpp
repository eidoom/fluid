#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>

// glad must be included before GLFW
#include <glad/glad.h>

// #include "large-fluid.h"
// #include "large-mesh.h"
#include "colourmap.h"
#include "shader.h"
#include "small-fluid.h"
#include "small-mesh.h"
#include "window.h"

#include <GLFW/glfw3.h>

const int SCALE = 10;
const int GRID_WIDTH = SCALE * 16;
const int GRID_HEIGHT = SCALE * 9;

int main()
{
    Window window(960, 540);

    Shader shader("shader.vert", "shader.frag");

    // LargeMesh mesh(GRID_SIZE, shader);
    SmallMesh<GRID_WIDTH, GRID_HEIGHT> mesh(shader);

    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // glClearColor(0.2f, 0.2f, 0.3f, 1.f);

    shader.use();

    // LargeFluid fluid(GRID_SIZE);
    SmallFluid<GRID_WIDTH, GRID_HEIGHT> fluid;

    float prev_time;
    float this_time { static_cast<float>(glfwGetTime()) };

    while (!glfwWindowShouldClose(window.window)) {

        if (glfwGetMouseButton(window.window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            double xpos, ypos;
            glfwGetCursorPos(window.window, &xpos, &ypos);
            fluid.addSource(static_cast<int>((xpos / window.width) * (GRID_WIDTH - 1)), static_cast<int>((1.f - ypos / window.height) * (GRID_HEIGHT - 1)));
        }

        if (glfwGetMouseButton(window.window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
            double xpos, ypos;
            glfwGetCursorPos(window.window, &xpos, &ypos);
            fluid.addSink(static_cast<int>((xpos / window.width) * (GRID_WIDTH - 1)), static_cast<int>((1.f - ypos / window.height) * (GRID_HEIGHT - 1)));
        }

        fluid.addFan(GRID_WIDTH / 4, GRID_HEIGHT / 2, 0.f, 1e6f);

        prev_time = this_time;
        this_time = static_cast<float>(glfwGetTime());
        fluid.tick(this_time - prev_time);

        for (std::size_t i { 0 }; i < mesh.vertices.size(); ++i) {
            mesh.vertices[i].colour = Colourmap::getColour(std::max(0.f, std::min(1.f, fluid.density()[i])));
        }

        /* Render start here */

        // glClear(GL_COLOR_BUFFER_BIT);

        mesh.draw();

        /* Render end here */

        window.tick();
    }

    glfwTerminate();
}
