GLAD_LCL=glad

CXX?=g++
CXXFLAGS=-O3 -std=c++17 -Wall -Wextra
CPPFLAGS=-I$(GLAD_LCL)/include
LDFLAGS=`pkg-config --libs glfw3 gl`

.PHONEY: all

all: main

main: main.o glad.o shader.o large-mesh.o vertex.o large-fluid.o colourmap.o window.o
	$(CXX) -o $@ $^ $(LDFLAGS)

glad.o: glad/src/glad.c $(GLAD_LCL)/include/glad/glad.h
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

large-fluid.o: large-fluid.cpp large-fluid.h matrix.h
	$(CXX) -c -o $@ $< $(CXXFLAGS)

window.o: window.cpp $(GLAD_LCL)/include/glad/glad.h window.h
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

shader.o: shader.cpp $(GLAD_LCL)/include/glad/glad.h shader.h shader.vert shader.frag
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

colourmap.o: colourmap.cpp colourmap.h
	$(CXX) -c -o $@ $< $(CXXFLAGS)

vertex.o: vertex.cpp vertex.h
	$(CXX) -c -o $@ $< $(CXXFLAGS)

large-mesh.o: large-mesh.cpp $(GLAD_LCL)/include/glad/glad.h large-mesh.h vertex.h matrix.h
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

main.o: main.cpp $(GLAD_LCL)/include/glad/glad.h shader.h large-mesh.h large-fluid.h small-mesh.h small-fluid.h colourmap.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)

$(GLAD_LCL)/src/glad.c $(GLAD_LCL)/include/glad/glad.h:
	mkdir -p $(GLAD_LCL)
	glad --generator c --no-loader --profile core --out-path $(GLAD_LCL)
