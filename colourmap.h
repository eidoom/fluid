#pragma once

#include <array>

class Colourmap {
public:
    static std::array<float, 3> getColour(float number);

private:
    static const std::array<std::array<float, 3>, 256> viridis;
};
