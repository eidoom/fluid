#include <cstdlib>
#include <iostream>

#include "window.h"

Window::Window(const int w, const int h)
    : width(w)
    , height(h)
{
    if (!glfwInit()) {
        std::cerr << "Failed to initialize GLFW" << '\n';
        std::exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_MAXIMIZED, GL_FALSE);

    window = glfwCreateWindow(width, height, "Hello Fluid", NULL, NULL);
    if (!window) {
        std::cerr << "Failed to create GLFW window" << '\n';
        glfwTerminate();
        std::exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cerr << "Failed to initialize GLAD" << '\n';
        std::exit(EXIT_FAILURE);
    }

    glViewport(0, 0, width, height);

    // glfwSetCursorPosCallback(window, cursor_position_callback);
    // glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetKeyCallback(window, key_callback);
}

void Window::tick()
{
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void error_callback(int error, const char* description)
{
    std::cerr
        << "GLFW callback error!" << '\n'
        << "Error code " << error << '\n'
        << description << '\n';
}

// void Window::cursor_position_callback(GLFWwindow* /* window */, double xpos, double ypos)
// {
//     std::cout << "Moved to (" << xpos << ", " << ypos << ")" << '\n';
// }

// void Window::mouse_button_callback(GLFWwindow* window, int button, int action, int /* mods */)
// {
//     if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
//         double xpos, ypos;
//         glfwGetCursorPos(window, &xpos, &ypos);
//         // fluid.addSource(xpos, ypos);
//         std::cout << "Left cllck at (" << xpos << ", " << ypos << ")" << '\n';
//     }
// }

void Window::key_callback(GLFWwindow* window, int key, int /* scancode */, int action, int /* mods */)
{
    if (key == GLFW_KEY_Q && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}
