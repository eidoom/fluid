#pragma once

#include <array>

#include <glad/glad.h>

#include <GLFW/glfw3.h>

#include "matrix.h"
#include "shader.h"
#include "vertex.h"

template <int W, int H>
class SmallMesh {
private:
    static constexpr int w { W - 1 };
    static constexpr int h { H - 1 };

    void setupMesh();
    void bufferVertices();

    Shader& shader;

    // Vertex Array Object, Vertex Buffer Object, Element Buffer Object
    unsigned int VAO, VBO, EBO;

public:
    SmallMesh(Shader& shader);

    ~SmallMesh();

    void draw();

    Matrix<Vertex, W, H> vertices;
    std::array<unsigned int, 6 * w * h> indices;
};

// --------------
// implementation
// --------------

template <int W, int H>
SmallMesh<W, H>::SmallMesh(Shader& shader)
    : shader(shader)
    , VAO()
    , VBO()
    , EBO()
{
    float dx { 2.f / w };
    float dy { 2.f / h };
    for (int y { 0 }; y < H; ++y) {
        for (int x { 0 }; x < W; ++x) {
            vertices.ref(x, y) = Vertex(-1.f + dx * x, -1.f + dy * y);
        }
    }

    int a { 0 };
    for (int y { 0 }; y < h; ++y) {
        for (int x { 0 }; x < w; ++x) {
            int i { vertices.i(x, y) };
            indices[a++] = i;         // bottom left
            indices[a++] = i + 1;     // bottom right
            indices[a++] = i + W + 1; // top right
            indices[a++] = i;         // bottom left
            indices[a++] = i + W + 1; // top right
            indices[a++] = i + W;     // top left
        }
    }

    setupMesh();
}

template <int W, int H>
SmallMesh<W, H>::~SmallMesh()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

template <int W, int H>
void SmallMesh<W, H>::bufferVertices()
{
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);
}

template <int W, int H>
void SmallMesh<W, H>::setupMesh()
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    bufferVertices();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices.data(), GL_STATIC_DRAW);

    // position attribute
    // vertex attribute: (location, size, type, normalise, stride, offset)
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);

    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);
}

template <int W, int H>
void SmallMesh<W, H>::draw()
{
    bufferVertices();

    // indices: (mode, count, type, offset)
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
}
