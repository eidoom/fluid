#pragma once

#include <array>
#include <vector>

template <typename T, int W, int H>
class Matrix : public std::array<T, W * H> {
public:
    Matrix();

    int i(int x, int y) const;

    T& ref(int x, int y);
    T get(int x, int y) const;
};

template <typename T>
class LargeMatrix : public std::vector<T> {
public:
    LargeMatrix(int w_, int h);

    int i(int x, int y) const;

    T& ref(int x, int y);
    T get(int x, int y) const;

private:
    int w;
};

template <typename T, int L>
class SquareMatrix : public Matrix<T, L, L> {
public:
    SquareMatrix();
};

template <typename T>
class LargeSquareMatrix : public LargeMatrix<T> {
public:
    LargeSquareMatrix(int l);
};

// --------------
// implementation
// --------------

// Matrix

template <typename T, int W, int H>
Matrix<T, W, H>::Matrix()
    : std::array<T, W * H>()
{
}

template <typename T, int W, int H>
int Matrix<T, W, H>::i(const int x, const int y) const
{
    return y * W + x;
}

template <typename T, int W, int H>
T& Matrix<T, W, H>::ref(const int x, const int y)
{
    return (*this)[i(x, y)];
}

template <typename T, int W, int H>
T Matrix<T, W, H>::get(const int x, const int y) const
{
    return (*this)[i(x, y)];
}

// LargeMatrix

template <typename T>
LargeMatrix<T>::LargeMatrix(int w_, int h)
    : std::vector<T>(w_ * h)
    , w(w_)
{
}

template <typename T>
int LargeMatrix<T>::i(const int x, const int y) const
{
    return y * w + x;
}

template <typename T>
T& LargeMatrix<T>::ref(const int x, const int y)
{
    return (*this)[i(x, y)];
}

template <typename T>
T LargeMatrix<T>::get(const int x, const int y) const
{
    return (*this)[i(x, y)];
}

// SquareMatrix

template <typename T, int L>
SquareMatrix<T, L>::SquareMatrix()
    : Matrix<T, L, L>()
{
}

// LargeSquareMatrix

template <typename T>
LargeSquareMatrix<T>::LargeSquareMatrix(int l)
    : LargeMatrix<T>(l, l)
{
}
