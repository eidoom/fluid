#pragma once

#include <array>

class Vertex {
public:
    Vertex(const std::array<float, 2>& position_, const std::array<float, 3>& colour_);
    Vertex(const std::array<float, 2>& position_);
    Vertex(float x, float y, float r, float g, float b);
    Vertex(float x, float y);
    Vertex();

    std::array<float, 2> position;
    std::array<float, 3> colour;
};
