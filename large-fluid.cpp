#include <cstdlib>

#include "large-fluid.h"

LargeFluid::LargeFluid(int N_)
    : N(N_)
    , n(N_-2)
    , dens_prev(N_)
    , dens(N_)
    , velx_prev(N_)
    , velx(N_)
    , vely_prev(N_)
    , vely(N_)
    , diff(1.f)
    , visc(1.f)
    , iterations(20)
{
    dens.ref(50, 50) = 5e3f;
}

const LargeSquareMatrix<float>& LargeFluid::density() const
{
    return dens;
}

void LargeFluid::addSource(float dt, const Grid& source, Grid& grid)
{
    for (std::size_t i { 0 }; i < source.size(); ++i) {
        grid[i] += dt * source[i];
    }
}

// iterative solver (Gauss-Seidel relaxation) for
// x0[i,j]=x[i,j]-a*(x[i-1,j]+x[i+1,j]+x[i,j-1]+x[i,j+1]-4*x[i,j]);
// https://en.wikipedia.org/wiki/Gauss%E2%80%93Seidel_method
void LargeFluid::diffuse(float dt, const Grid& grid_prev, Grid& grid, float coeff)
{
    float a { dt * coeff * n * n };
    float c { 1.f / (1.f + 4.f * a) };
    for (int k { 0 }; k < iterations; ++k) {
        for (int i { 1 }; i <= n; ++i) {
            for (int j { 1 }; j <= n; ++j) {
                grid.ref(i, j) = c * (grid_prev.get(i, j) + a * (grid.get(i - 1, j) + grid.get(i + 1, j) + grid.get(i, j - 1) + grid.get(i, j + 1)));
            }
        }
    }
}

void LargeFluid::advect(float dt, const Grid& grid_prev, Grid& grid, const Grid& u, const Grid& v)
{
    float dt0 { dt * n };
    for (int i { 1 }; i <= n; ++i) {
        for (int j { 1 }; j <= n; ++j) {
            float x { i - dt0 * u.get(i, j) };
            float y { j - dt0 * v.get(i, j) };

            if (x < 0.5f) {
                x = 0.5f;
            } else if (x > n + 0.5f) {
                x = n + 0.5f;
            }

            if (y < 0.5f) {
                y = 0.5f;
            } else if (y > n + 0.5f) {
                y = n + 0.5f;
            }

            int i0 { static_cast<int>(x) };
            int i1 { i0 + 1 };

            int j0 { static_cast<int>(y) };
            int j1 { j0 + 1 };

            float s1 { x - i0 };
            float s0 { 1.f - s1 };
            float t1 { y - j0 };
            float t0 { 1.f - t1 };

            grid.ref(i, j) = s0 * (t0 * grid_prev.get(i0, j0) + t1 * grid_prev.get(i0, j1))
                + s1 * (t0 * grid_prev.get(i1, j0) + t1 * grid_prev.get(i1, j1));
        }
    }
}

void LargeFluid::project()
{
    float h { 1.f / n };
    for (int i { 1 }; i <= n; ++i) {
        for (int j { 1 }; j <= n; ++j) {
            vely_prev.ref(i, j) = -0.5f * h * (velx.get(i + 1, j) - velx.get(i - 1, j) + vely.get(i, j + 1) - vely.get(i, j - 1));
            velx_prev.ref(i, j) = 0.f;
        }
    }
    setBoundary(vely_prev);
    setBoundary(velx_prev);
    for (int k { 0 }; k < iterations; ++k) {
        for (int i { 1 }; i <= n; ++i) {
            for (int j { 1 }; j <= n; ++j) {
                velx_prev.ref(i, j) = 0.25f * (vely_prev.get(i, j) + velx_prev.get(i - 1, j) + velx_prev.get(i + 1, j) + velx_prev.get(i, j - 1) + velx_prev.get(i, j + 1));
            }
        }
        setBoundary(velx_prev);
    }
    for (int i { 1 }; i <= n; ++i) {
        for (int j { 1 }; j <= n; ++j) {
            velx.ref(i, j) -= 0.5f * (velx_prev.get(i + 1, j) - velx_prev.get(i - 1, j)) * n;
            vely.ref(i, j) -= 0.5f * (velx_prev.get(i, j + 1) - velx_prev.get(i, j - 1)) * n;
        }
    }
    setBoundaryVelX();
    setBoundaryVelY();
}

void LargeFluid::setBoundary(Grid& grid)
{
    for (int i { 1 }; i <= n; ++i) {
        grid.ref(0, i) = grid.get(1, i);
        grid.ref(n + 1, i) = grid.get(n, i);
        grid.ref(i, 0) = grid.get(i, 1);
        grid.ref(i, n + 1) = grid.get(i, n);
    }
    setBoundaryCorners(grid);
}

void LargeFluid::setBoundaryVelX()
{
    for (int i { 1 }; i <= n; ++i) {
        velx.ref(0, i) = -velx.get(1, i);
        velx.ref(n + 1, i) = -velx.get(n, i);
        velx.ref(i, 0) = velx.get(i, 1);
        velx.ref(i, n + 1) = velx.get(i, n);
    }
    setBoundaryCorners(velx);
}

void LargeFluid::setBoundaryVelY()
{
    for (int i { 1 }; i <= n; ++i) {
        vely.ref(0, i) = vely.get(1, i);
        vely.ref(n + 1, i) = vely.get(n, i);
        vely.ref(i, 0) = -vely.get(i, 1);
        vely.ref(i, n + 1) = -vely.get(i, n);
    }
    setBoundaryCorners(vely);
}

void LargeFluid::setBoundaryCorners(Grid& grid)
{
    grid.ref(0, 0) = 0.5f * (grid.get(1, 0) + grid.get(0, 1));
    grid.ref(0, n + 1) = 0.5f * (grid.get(1, n + 1) + grid.get(0, n));
    grid.ref(n + 1, 0) = 0.5f * (grid.get(n, 0) + grid.get(n + 1, 1));
    grid.ref(n + 1, n + 1) = 0.5f * (grid.get(n, n + 1) + grid.get(n + 1, n));
}

void LargeFluid::tick(float dt)
{
    // velocity
    addSource(dt, velx_prev, velx);
    std::swap(velx_prev, velx);
    diffuse(dt, velx_prev, velx, visc);
    setBoundaryVelX();

    addSource(dt, vely_prev, vely);
    std::swap(vely_prev, vely);
    diffuse(dt, vely_prev, vely, visc);
    setBoundaryVelY();

    project();

    std::swap(velx_prev, velx);
    std::swap(vely_prev, vely);

    advect(dt, velx_prev, velx, velx_prev, vely_prev);
    setBoundaryVelX();
    advect(dt, vely_prev, vely, velx_prev, vely_prev);
    setBoundaryVelY();

    project();

    // density
    addSource(dt, dens_prev, dens);
    std::swap(dens_prev, dens);
    diffuse(dt, dens_prev, dens, diff);
    setBoundary(dens);
    std::swap(dens_prev, dens);
    advect(dt, dens_prev, dens, velx, vely);
    setBoundary(dens);
}
