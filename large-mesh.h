#pragma once

#include <vector>

#include "matrix.h"
#include "shader.h"
#include "vertex.h"

class LargeMesh {
private:
    const int L;
    const int l;

    void setupMesh();
    void bufferVertices();

    Shader& shader;

    // Vertex Array Object, Vertex Buffer Object, Element Buffer Object
    unsigned int VAO, VBO, EBO;
public:
    LargeMesh(int L_, Shader& shader);

    ~LargeMesh();

    void draw();

    LargeSquareMatrix<Vertex> vertices;
    std::vector<unsigned int> indices;
};
